using EfsolPoll.Data;
using EfsolPoll.Data.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace EfsolPoll.Controllers
{
    [TestClass]
    public class PollControllerTests
    {
        [TestMethod]
        public async Task Get_ReturnsCorrectData()
        {
            var dbContextOptions = new DbContextOptionsBuilder<PollDbContext>()
                .UseInMemoryDatabase("PollDb")
                .Options;

            var dbContext = new PollDbContext(dbContextOptions);

            dbContext.Database.EnsureCreated();

            var controller = new PollController(dbContext, Mock.Of<ILogger<PollController>>());
            var result = await controller.Get();

            var questions = result.Value.Questions;

            Assert.AreEqual(6, questions.Length);

            Assert.AreEqual("text", questions[0].Type);
            Assert.AreEqual("������� ���", questions[0].Text);

            Assert.AreEqual("integer", questions[1].Type);
            Assert.AreEqual("������� �������", questions[1].Text);

            Assert.AreEqual("enumeration", questions[2].Type);
            Assert.AreEqual("������� ���", questions[2].Text);

            Assert.AreEqual("date", questions[3].Type);
            Assert.AreEqual("������� ���� ��������", questions[3].Text);

            Assert.AreEqual("enumeration", questions[4].Type);
            Assert.AreEqual("������� �������� ���������", questions[4].Text);

            Assert.AreEqual("logical", questions[5].Type);
            Assert.AreEqual("������ �� �� ���������������", questions[5].Text);
        }

        [TestMethod]
        public async Task Get_DoesCorrectUpdates_ForNewVisitor()
        {
            var ipAddress = "123.123.123.123";
            var httpContextMock = new Mock<HttpContext>();
            var connectionInfoMock = new Mock<ConnectionInfo>();

            httpContextMock.Setup(c => c.Connection).Returns(connectionInfoMock.Object);
            connectionInfoMock.Setup(i => i.RemoteIpAddress).Returns(IPAddress.Parse(ipAddress));

            var dbContextOptions = new DbContextOptionsBuilder<PollDbContext>()
                .UseInMemoryDatabase("PollDb")
                .Options;

            var dbContext = new PollDbContext(dbContextOptions);

            dbContext.Database.EnsureCreated();

            var controller = new PollController(dbContext, Mock.Of<ILogger<PollController>>())
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContextMock.Object
                }
            };

            await controller.Post(new PollController.PostRequestData
            {
                Answers = new[]
                {
                    new PollController.PostRequestData.Answer
                    {
                        Text = "���� ������"
                    },
                    new PollController.PostRequestData.Answer
                    {
                    },
                    new PollController.PostRequestData.Answer
                    {
                    },
                    new PollController.PostRequestData.Answer
                    {
                    },
                    new PollController.PostRequestData.Answer
                    {
                    },
                    new PollController.PostRequestData.Answer
                    {
                        Logical = true
                    }
                }
            });

            var questions = await dbContext.Set<Question>()
                .ToListAsync();

            var question1 = questions.Single(q => q.Ordinal == 1);
            var question2 = questions.Single(q => q.Ordinal == 2);
            var question3 = questions.Single(q => q.Ordinal == 3);
            var question4 = questions.Single(q => q.Ordinal == 4);
            var question5 = questions.Single(q => q.Ordinal == 5);
            var question6 = questions.Single(q => q.Ordinal == 6);

            var answerSet = await dbContext.Set<AnswerSet>()
                .Include(s => s.Answers)
                .FirstOrDefaultAsync(s => s.IpAddress == ipAddress);

            Assert.IsNotNull(answerSet);
            Assert.AreEqual(ipAddress, answerSet.IpAddress);

            CollectionAssert.AreEquivalent(
                new[] { question1, question2, question3, question4, question5, question6 },
                answerSet.Answers.Select(a => a.Question).ToArray());

            var answer1 = answerSet.Answers.Single(a => a.Question == question1);
            var answer2 = answerSet.Answers.Single(a => a.Question == question2);
            var answer3 = answerSet.Answers.Single(a => a.Question == question3);
            var answer4 = answerSet.Answers.Single(a => a.Question == question4);
            var answer5 = answerSet.Answers.Single(a => a.Question == question5);
            var answer6 = answerSet.Answers.Single(a => a.Question == question6);

            Assert.IsInstanceOfType(D(answer1.Content), typeof(TextAnswerContent));
            Assert.AreEqual("���� ������", ((TextAnswerContent)D(answer1.Content)).Value);

            Assert.IsInstanceOfType(D(answer2.Content), typeof(IntegerAnswerContent));

            Assert.IsInstanceOfType(D(answer3.Content), typeof(EnumerationAnswerContent));

            Assert.IsInstanceOfType(D(answer4.Content), typeof(DateAnswerContent));

            Assert.IsInstanceOfType(D(answer5.Content), typeof(EnumerationAnswerContent));

            Assert.IsInstanceOfType(D(answer6.Content), typeof(LogicalAnswerContent));
            Assert.AreEqual(true, ((LogicalAnswerContent)D(answer6.Content)).Value);
        }

        private static AnswerContent D(string s)
        {
            return JsonConvert.DeserializeObject<AnswerContent>(s, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            });
        }
    }
}

﻿using EfsolPoll.Data.Json;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Diagnostics.CodeAnalysis;

namespace EfsolPoll.Data
{
    public class PollDbContext : DbContext
    {
        public PollDbContext()
        {
        }

        public PollDbContext([NotNull] DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Question>().HasKey(o => o.Id);
            modelBuilder.Entity<AnswerSet>().HasKey(o => o.Id);
            modelBuilder.Entity<AnswerSet>().HasMany(o => o.Answers).WithOne(o => o.Set);
            modelBuilder.Entity<Answer>().HasKey(o => o.Id);
            modelBuilder.Entity<Answer>().HasOne(o => o.Set).WithMany(o => o.Answers).HasForeignKey(o => o.SetId);
            modelBuilder.Entity<Answer>().HasOne(o => o.Question).WithMany().HasForeignKey(o => o.QuestionId);

            var jsonSerializerSettings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };

            modelBuilder.Entity<Question>().HasData(
                new Question
                {
                    Id = Guid.NewGuid(),
                    Ordinal = 1,
                    Content = JsonConvert.SerializeObject(
                        new TextQuestionContent
                        {
                            Text = "Введите имя"
                        },
                        jsonSerializerSettings)
                },
                new Question
                {
                    Id = Guid.NewGuid(),
                    Ordinal = 2,
                    Content = JsonConvert.SerializeObject(
                        new IntegerQuestionContent
                        {
                            Text = "Введите возраст"
                        },
                        jsonSerializerSettings)
                },
                new Question
                {
                    Id = Guid.NewGuid(),
                    Ordinal = 3,
                    Content = JsonConvert.SerializeObject(
                        new EnumerationQuestionContent
                        {
                            Text = "Введите пол"
                        },
                        jsonSerializerSettings)
                },
                new Question
                {
                    Id = Guid.NewGuid(),
                    Ordinal = 4,
                    Content = JsonConvert.SerializeObject(
                        new DateQuestionContent
                        {
                            Text = "Введите дату рождения"
                        },
                        jsonSerializerSettings)
                },
                new Question
                {
                    Id = Guid.NewGuid(),
                    Ordinal = 5,
                    Content = JsonConvert.SerializeObject(
                        new EnumerationQuestionContent
                        {
                            Text = "Введите семейное положение"
                        },
                        jsonSerializerSettings)
                },
                new Question
                {
                    Id = Guid.NewGuid(),
                    Ordinal = 6,
                    Content = JsonConvert.SerializeObject(
                        new LogicalQuestionContent
                        {
                            Text = "Любите ли вы программировать"
                        },
                        jsonSerializerSettings)
                });
        }
    }
}

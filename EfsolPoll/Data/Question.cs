﻿using System;

namespace EfsolPoll.Data
{
    public class Question
    {
        public Guid Id
        {
            get;
            set;
        }

        public int Ordinal
        {
            get;
            set;
        }

        // JSON
        public string Content
        {
            get;
            set;
        }
    }
}

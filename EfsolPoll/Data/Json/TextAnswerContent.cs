﻿namespace EfsolPoll.Data.Json
{
    public class TextAnswerContent : AnswerContent
    {
        public string Value
        {
            get;
            set;
        }
    }
}

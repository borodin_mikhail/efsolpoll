﻿namespace EfsolPoll.Data.Json
{
    public class LogicalAnswerContent : AnswerContent
    {
        public bool Value
        {
            get;
            set;
        }
    }
}

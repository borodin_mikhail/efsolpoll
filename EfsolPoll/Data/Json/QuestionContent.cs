﻿namespace EfsolPoll.Data.Json
{
    public abstract class QuestionContent
    {
        public string Text
        {
            get;
            set;
        }
    }
}

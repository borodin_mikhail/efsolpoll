﻿using System;
using System.Collections.ObjectModel;

namespace EfsolPoll.Data
{
    public class AnswerSet
    {
        public Guid Id
        {
            get;
            set;
        }

        public string IpAddress
        {
            get;
            set;
        }

        public Collection<Answer> Answers
        {
            get;
            set;
        }
    }
}

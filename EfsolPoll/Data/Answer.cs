﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfsolPoll.Data
{
    public class Answer
    {
        public Guid Id
        {
            get;
            set;
        }

        public AnswerSet Set
        {
            get;
            set;
        }

        public Guid SetId
        {
            get;
            set;
        }

        public Question Question
        {
            get;
            set;
        }

        public Guid QuestionId
        {
            get;
            set;
        }

        // JSON
        public string Content
        {
            get;
            set;
        }
    }
}

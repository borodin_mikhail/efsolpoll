import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
})
export class HomeComponent {
    questions: QuestionState[] = [];

    currentQuestionIndex?: number;

    constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
        console.log(baseUrl);

        this.http.get<GetResponseData>(baseUrl + 'poll').subscribe(result => {
            this.questions = result.questions.map(q => {
                switch (q.type) {
                    case 'date':
                        return {
                            type: 'date',
                            text: q.text
                        };

                    case 'enumeration':
                        return {
                            type: 'enumeration',
                            text: q.text
                        };

                    case 'integer':
                        return {
                            type: 'integer',
                            text: q.text
                        };

                    case 'logical':
                        return {
                            type: 'logical',
                            text: q.text
                        };

                    case 'text':
                        return {
                            type: 'text',
                            text: q.text
                        };
                }
            });
            this.currentQuestionIndex = this.questions.length > 0 ? 0 : undefined;
        });
    }

    canSave(): boolean {
        return this.questions.every(q => {
            switch (q.type) {
                case 'date':
                    return true;

                case 'enumeration':
                    return true;

                case 'integer':
                    return true;

                case 'logical':
                    return q.answer !== undefined;

                case 'text':
                    return q.answer != '';
            }
        });
    }

    onPreviousClick(): void {
        if (this.currentQuestionIndex > 0) {
            --this.currentQuestionIndex;
        }
    }

    onNextClick(): void {
        if (this.currentQuestionIndex < this.questions.length - 1) {
            ++this.currentQuestionIndex;
        }
    }

    onSaveClick(): void {
        const data = this.getPostData();
        this.http.post(this.baseUrl + 'poll', data).subscribe(
            _ => {
                alert("Ответы сохранены!");
            },
            _ => {
                alert("Сохранить ответы не удалось!");
            });
    }

    private getPostData(): PostRequestData {
        return {
            answers: this.questions.map(q => {
                switch (q.type) {
                    case 'date':
                        return {
                        };

                    case 'enumeration':
                        return {
                        };

                    case 'integer':
                        return {
                        };

                    case 'logical':
                        return {
                            logical: q.answer
                        };

                    case 'text':
                        return {
                            text: q.answer
                        };
                }
            })
        }
    }
}

type QuestionState = DateQuestionState | EnumerationQuestionState | IntegerQuestionState | LogicalQuestionState | TextQuestionState;

type DateQuestionState = {
    type: 'date',
    text: string
}

type EnumerationQuestionState = {
    type: 'enumeration',
    text: string
}

type IntegerQuestionState = {
    type: 'integer',
    text: string
}

type LogicalQuestionState = {
    type: 'logical',
    text: string,
    answer?: boolean
}

type TextQuestionState = {
    type: 'text',
    text: string
    answer?: string
}

type GetResponseData = {
    questions: Question[]
};

type Question = DateQuestion | EnumerationQuestion | IntegerQuestion | LogicalQuestion | TextQuestion;

type DateQuestion = {
    type: 'date',
    text: string
}

type EnumerationQuestion = {
    type: 'enumeration',
    text: string
}

type IntegerQuestion = {
    type: 'integer',
    text: string
}

type LogicalQuestion = {
    type: 'logical',
    text: string
}

type TextQuestion = {
    type: 'text',
    text: string
}

type PostRequestData = {
    answers: Answer[]
}

type Answer = DateAnswer | EnumerationAnswer | IntegerAnswer | LogicalAnswer | TextAnswer;

type DateAnswer = {};

type EnumerationAnswer = {};

type IntegerAnswer = {};

type LogicalAnswer = {
    logical: boolean;
};

type TextAnswer = {
    text: string;
};

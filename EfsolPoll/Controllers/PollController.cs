﻿using EfsolPoll.Data;
using EfsolPoll.Data.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EfsolPoll.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PollController : ControllerBase
    {
        private readonly PollDbContext _dbContext;

        private readonly ILogger<PollController> _logger;

        private static readonly JsonSerializerSettings _jsonSerializerSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.All
        };

        public PollController(PollDbContext dbContext, ILogger<PollController> logger)
        {
            _dbContext = dbContext;
            _logger = logger;

            _dbContext.Database.EnsureCreated(); // Should be removed in production.
        }

        [HttpGet]
        public async Task<ActionResult<GetResponseData>> Get()
        {
            var questions = await _dbContext.Set<Question>().OrderBy(q => q.Ordinal).ToListAsync();

            return new GetResponseData
            {
                Questions = questions.Select(
                    q => (DeserializeQuestionContent(q.Content)) switch
                    {
                        DateQuestionContent content => new GetResponseData.Question
                        {
                            Type = "date",
                            Text = content.Text
                        },

                        EnumerationQuestionContent content => new GetResponseData.Question
                        {
                            Type = "enumeration",
                            Text = content.Text
                        },

                        IntegerQuestionContent content => new GetResponseData.Question
                        {
                            Type = "integer",
                            Text = content.Text
                        },

                        LogicalQuestionContent content => new GetResponseData.Question
                        {
                            Type = "logical",
                            Text = content.Text
                        },

                        TextQuestionContent content => new GetResponseData.Question
                        {
                            Type = "text",
                            Text = content.Text
                        },

                        _ => throw new Exception(),
                    })
                .ToArray()
            };
        }

        public async Task<ActionResult> Post([FromBody] PostRequestData data)
        {
            var ipAddress = HttpContext.Connection.RemoteIpAddress.ToString();

            // Уже сохраняли результаты?
            if (await _dbContext.Set<AnswerSet>().AnyAsync(s => s.IpAddress == ipAddress))
            {
                // Повторно сохранять результаты опроса запрещено (не поддерживается).
                return Forbid();
            }

            var questions = await _dbContext.Set<Question>().OrderBy(q => q.Ordinal).ToListAsync();

            var answers = data.Answers;

            // Минимальная проверка валидности того, что получено.
            if (answers.Length != questions.Count)
            {
                return BadRequest();
            }

            var answerSet = new AnswerSet
            {
                Id = Guid.NewGuid(),
                IpAddress = ipAddress
            };

            await _dbContext.Set<AnswerSet>().AddAsync(answerSet);

            await _dbContext.Set<Answer>().AddRangeAsync(questions.Zip(answers).Select(t =>
            {
                var (question, answer) = t;
                var questionContent = DeserializeQuestionContent(question.Content);
                var answerContent = GetAnswerContent(questionContent, answer);

                return new Answer
                {
                    Id = Guid.NewGuid(),
                    Question = question,
                    Set = answerSet,
                    Content = Serialize(answerContent)
                };
            }));

            await _dbContext.SaveChangesAsync();

            return NoContent();
        }

        private static string Serialize(AnswerContent content)
        {
            return JsonConvert.SerializeObject(content, _jsonSerializerSettings);
        }

        private static QuestionContent DeserializeQuestionContent(string s)
        {
            return JsonConvert.DeserializeObject<QuestionContent>(s, _jsonSerializerSettings);
        }

        private AnswerContent GetAnswerContent(QuestionContent baseContent, PostRequestData.Answer answer)
        {
            switch (baseContent)
            {
                case DateQuestionContent content:
                    return new DateAnswerContent
                    {
                    };

                case EnumerationQuestionContent content:
                    return new EnumerationAnswerContent
                    {
                    };

                case IntegerQuestionContent content:
                    return new IntegerAnswerContent
                    {
                    };

                case LogicalQuestionContent content:
                    return new LogicalAnswerContent
                    {
                        Value = answer.Logical
                    };

                case TextQuestionContent content:
                    return new TextAnswerContent
                    {
                        Value = answer.Text
                    };

                default:
                    throw new Exception();
            }
        }

        public class GetResponseData
        {
            public Question[] Questions
            {
                get;
                set;
            }

            public class Question
            {
                public string Type
                {
                    get;
                    set;
                }

                public string Text
                {
                    get;
                    set;
                }
            }
        }

        public class PostRequestData
        {
            public Answer[] Answers
            {
                get;
                set;
            }

            public class Answer
            {
                public bool Logical
                {
                    get;
                    set;
                }

                public string Text
                {
                    get;
                    set;
                }
            }
        }
    }
}
